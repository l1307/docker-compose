create schema fizz;

create tablespace  fizz_index location '/base/postgresql/tablespace/fizz-index' ;
create tablespace  fizz_data location '/base/postgresql/tablespace/fizz-data' ;
