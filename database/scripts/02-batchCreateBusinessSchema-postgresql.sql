drop table if exists fizz_buzz_metrics cascade;

drop sequence if exists fizz_buzz_metrics_seq;

-- ===============================================
-- table fizz_buzz_metrics
-- ===============================================
create sequence fizz_buzz_metrics_seq start with 1;
create table fizz_buzz_metrics (
fizz_buzz_metrics_id bigint default nextval('fizz_buzz_metrics_seq') not null,
fizz_buzz_limit smallint not null,
fizz_buzz_int_1 smallint not null,
fizz_buzz_int_2 smallint not null,
fizz_buzz_str_1 varchar (32) not null,
fizz_buzz_str_2 varchar (32) not null,
fizz_buzz_count smallint not null
);
alter table fizz_buzz_metrics add constraint fizz_buzz_metrics_pk primary key ( fizz_buzz_metrics_id ) ;
alter table fizz_buzz_metrics add constraint fizz_buzz_metrics_un unique ( fizz_buzz_limit, fizz_buzz_int_1, fizz_buzz_int_2, fizz_buzz_str_1, fizz_buzz_str_2) ;

